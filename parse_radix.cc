#include<iostream>
#include<string>
#include<stdlib.h>
using namespace std;

int jagop(int n,int b);

int ParseRadix(const string& number);

string ConvertRadix(int number, int radix);

string convertInt(int number);

int main() {
  string ord,cal,num1,num2,num0="";
  int lnum,rnum,num,minus;
  cin>>ord;
  while(ord=="eval") {
    lnum=0;
    rnum=0;
    num0="";
    cin>>num1>>cal>>num2;
    if(cal!="=="&&cal!=">"&&cal!="<") cin>>num;
    lnum=ParseRadix(num1);
    rnum=ParseRadix(num2);
    if(lnum==-1||rnum==-1) {cout<<"Error"<<endl; cin>>ord;continue;}
    if(num!=10) { num0+='_';num0+=convertInt(num);}
    if(cal=="+")cout<<ConvertRadix(rnum+lnum,num)<<num0<<endl;
    if(cal=="-"){
	if((lnum-rnum)<0)
		cout<<"-"<<ConvertRadix(rnum-lnum,num)<<num0<<endl;
	else cout<<ConvertRadix(lnum-rnum,num)<<num0<<endl;
    }
    if(cal=="*")cout<<ConvertRadix(rnum*lnum,num)<<num0<<endl;
    if(cal=="/")cout<<ConvertRadix(lnum/rnum,num)<<num0<<endl;
    if(cal=="=="){
      if(rnum==lnum)
	cout<<"true"<<endl;
      else
        cout<<"false"<<endl;
    }
    if(cal==">"){
      if(lnum>rnum)
	cout<<"true"<<endl;
      else
        cout<<"false"<<endl;
    }
    if(cal=="<"){
      if(lnum<rnum)
	cout<<"true"<<endl;
      else
        cout<<"false"<<endl;
    }

    cin>>ord;
  }
  return 0;

}

int ParseRadix(const string& number) {
    string lnum_,rnum_;
    int lnum,rnum,num=0;
    lnum_=number.substr(0,number.find('_'));
    if(number.find('_')!=string::npos)
      rnum_=number.substr(number.find('_')+1);
    else rnum_="10";
      rnum=atoi(rnum_.c_str());
    for(int i=0; i<lnum_.size(); i++) {
	if(lnum_[i]<58&&lnum_[i]>48+rnum)return -1;
        if(lnum_[i]>87+rnum)return -1;
	if(lnum_[i]<58)
	  num=num+(lnum_[i]-48)*jagop(lnum_.size()-1-i,rnum);
        else num=num+(lnum_[i]-87)*jagop(lnum_.size()-1-i,rnum);
    }
    return num;
}

string ConvertRadix(int number, int radix) {
    string num,rnum;
    int n=0;
    char k='a';
    while(number/radix!=0||number%radix!=0) {
	if(number%radix<10)
		num+=convertInt(number%radix);
	else {
		k+=number%radix-10;
		num+=k;
	}
	number=number/radix;
    }
    for (std::string::reverse_iterator rit=num.rbegin(); rit!=num.rend(); ++rit)
    rnum+=*rit;
    return rnum;
}

string convertInt(int number)
{
    if (number == 0)
        return "0";
    string temp="";
    string returnvalue="";
    while (number>0)
    {
        temp+=number%10+48;
        number/=10;
    }
    for (int i=0;i<temp.length();i++)
        returnvalue+=temp[temp.length()-i-1];
    return returnvalue;
}
int jagop(int n,int b) {
  int result=1;
  for(int i=0; i<n; i++)
	result=result*b;
  return result;
}
