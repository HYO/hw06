#include <iostream>
#include "point2d.h"
#include <vector>

Point::Point() {
}

Point::Point(const Point& p) {
  x=p.x;
  y=p.y;
}

Point::Point(int c){
  x=c;
  y=c;
}

Point::Point(int x, int y):x(x),y(y) {}

Point Point::operator-() const {
  return Point(-x,-y);
}

Point& Point::operator+=(const Point& p) {
  x=x+p.x;
  y=y+p.y;
  return *this;
}

Point& Point::operator-=(const Point& p) {
  x=x-p.x;
  y=y-p.y;
  return *this;
}

Point& Point::operator*=(const Point& p) {
  x=x*p.x;
  y=y*p.y;
  return *this;
}

Point operator+(const Point& lhs, const Point& rhs) {
  return Point(lhs.x+rhs.x,lhs.y+rhs.y);
}

Point operator-(const Point& lhs, const Point& rhs) {
  return Point(lhs.x-rhs.x,lhs.y-rhs.y);
}

Point operator*(const Point& lhs, const Point& rhs) {
  return Point(lhs.x*rhs.x,lhs.y*rhs.y);
}

std::istream& operator>>(std::istream& is, Point& pt) {
  return is;
}

std::ostream& operator<<(std::ostream& os, const Point& pt) {
  return os;
}

