// omok.h

#ifndef _OMOK_H_
#define _OMOK_H_

#include <iostream>
#include <vector>

class Omok {
 public:
  Omok();

  bool Put(int x, int y);
  
  bool IsOmok(bool* is_winner_black) const;
    
  bool IsBlacksTurn() const;
    
 private:
  // Define your private members here.
};      
                                   
std::ostream& operator<<(std::ostream& os, const Omok& omok);

#endif  // _OMOK_H_
