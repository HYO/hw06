// library.h

#ifndef _LIBRARY_H_
#define _LIBRARY_H_

#include <iostream>
#include <string>
#include <vector>

class Book {
 public:
  Book() {}
  Book(const Book& book) {total_=book.total(); stock_=book.stock();title_=book.title();}
  Book(const std::string& title, int total) {
    total_=total; stock_=total;title_=title;
  }
  const std::string& title() const {return title_;}  // 도서의 제목
  int total() const {return total_;}  // 해당 도서의 총 보유 권수
  int stock() const {return stock_;}  // 도서의 재고 (남아있는 권수)

  bool Lend(int count = 1);
  bool Return(int count = 1);

 private:
  int total_,stock_;
  string title_;
};

class Magazine : public Book {
 public:
  Magazine() {}
  Magazine(const Magazine& magazine) {total_=magazine.total(); stock_=magazine.stock();title_=magazine.title();}
  Magazine(const std::string& title, int total, int year, int month) {
    total_=total; stock_=total; title_=title; year_=year; month_=month;
  }

  // 잡지의 제목 'PCMagazine(13/9)' 처럼 년/월을 원래 제목에 추가하여 리턴.
  std::string title() const{
    return title_+'('+(char)year_+"/"+(char)month_+')';
  }
 private:
  int total_,stock_,year_,month_;
  string title_;
};

// Input을 처리하는 operator 구현.
std::istream& operator>>(std::istream& is, Book& book) {
	return is;
}
std::istream& operator>>(std::istream& is, Magazine& magazine) {
	return is;
}

class Library {
 public:
  Library();  // 디폴트 생성자, 카운트를 0으로 셋팅
  ~Library();  // 소멸자, 메모리 해제
  // 도서 정보를 추가하는 함수
  void AddBook(const std::string& title, int num);
  // 도서 정보를 삭제하는 함수
  void DeleteBook(const std::string& title);

  void Lend(const std::string& title); // 책 한권을 대여하는 함수
  void Return(const std::string& title); // 책 한권을 반납하는 함수
  void PrintAll(); // 현재 저장되어 있는 도서(책과 잡지)의 리스트를 출력.
                   // 출력형식: stock/total title
 private:
  vector<Book> books_;  // 도서를 저장하는 배열
  vector<Magazine> magazines_;  // 잡지를 저장하는 배열 
};
#endif  // _LIBRARY_H_
