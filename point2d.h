// point2d.h

#ifndef _POINT2D_H_
#define _POINT2D_H_

#include <iostream>

struct Point {
  int x, y;  // 멤버 변수.

  Point();
  Point(const Point& p);
  explicit Point(int c);
  Point(int x, int y);

  Point operator-() const;  // 전위 - 연산자
  Point& operator+=(const Point& p);
  Point& operator-=(const Point& p);
  Point& operator*=(const Point& p);
};

Point operator+(const Point& lhs, const Point& rhs);
Point operator-(const Point& lhs, const Point& rhs);
Point operator*(const Point& lhs, const Point& rhs);

std::istream& operator>>(std::istream& is, Point& pt);
std::ostream& operator<<(std::ostream& os, const Point& pt);

#endif  // _POINT2D_H_
